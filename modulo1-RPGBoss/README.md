
Download do jogo esta localizado na pasta "o_jogo/export"

## Regras (no geral)

**1.** O jogo deve possuir 3 cenários, sendo pelo menos um deles um campo aberto e um deles um mapa fechado.

**2.** Deve ser possível o jogador entrar e sair dos três mapas.

**3.** Utilizar os assets disponibilizados pela plataforma.

**4.** Os mapas devem possuir música de fundo.

**5.** No mapa interno deve haver um NPC com que o personagem possa ter um diálogo.

    * O NPC não deve ficar parado.

**6.** Na segunda vez que o jogador conversar com o NPC, ele deve propor um desafio e se o jogador ganhar, ele recebe um prémio, caso ele falhe, deve iniciar uma batalha.